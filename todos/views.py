from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ListForm, ItemForm

# Create your views here.


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list_object = get_object_or_404(TodoList, id=id)
    context = {
        "list_object": list_object,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            list_object = form.save()
            return redirect("todo_list_detail", id=list_object.id)
    else:
        form = ListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list_object = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=list_object)
        if form.is_valid():
            list_object = form.save()
            return redirect("todo_list_detail", id=list_object.id)
    else:
        form = ListForm(instance=list_object)

    context = {
        "form": form
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    list_object = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_object.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    form = ItemForm(request.POST)
    if form.is_valid():
        item_object = form.save()
        return redirect("todo_list_detail", id=item_object.list.id)
    else:
        form = ItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    item_object = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item_object)
        if form.is_valid():
            item_object = form.save()
            return redirect("todo_list_detail", id=item_object.list.id)
    else:
        form = ItemForm(instance=item_object)

    context = {
        "form": form,
    }
    return render(request, "todos/update_item.html", context)
